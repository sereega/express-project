const {
   saveData,
   deleteData
} = require("../repositories/user.repository");

const getId = (user) => {
   if (user) {
      return user.id;
   } else {
      return null;
   }
};

const getArray = (user) => {
   if (user) {
      return user;
   } else {
      return null;
   }
};

const getName = (user) => {
   if (user) {
      return user.name;
   } else {
      return null;
   }
};

const deleteUser = (user) => {
   if (user) {
      return deleteData(user.id);
   } else {
      return null;
   }
};

// const saveName = (user) => {
//    if (user) {
//       return saveData(user.name);
//    } else {
//       return null;
//    }
// };


module.exports = {
   getName,
   // saveName,
   getId,
   getArray,
   deleteUser,
   saveData
};