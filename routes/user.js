var express = require('express');
var router = express.Router();

const {
   getId,
   getArray,
   getName
} = require("../services/user.service");
const {
   saveData,
   deleteData,
   modifyUser
} = require('../repositories/user.repository');

let users = require('../api-users/userlist.json');

// BEGIN get an id of one user BEGIN
router.get('/:id', function (req, res, next) {
   const id = getId(req.params);
   const result = users[id - 1];
   if (result) {
      res.send(result);
   } else {
      res.status(400).send(`Some error`);
   }
});
// END get an id of one user END 

// BEGIN get an array of users BEGIN
router.get('/', function (req, res, next) {
   const result = getArray(users);
   if (result) {
      res.send(result);
   } else {
      res.status(400).send(`Some error`);
   }
});
// END get an array of users END 

// BEGIN create user BEGIN
router.post('/', function (req, res, next) {
   const result = saveData(req.body);
   if (result) res.send(`User ${req.body.name} was added successfully`);
   else res.status(400).send(`Invalid data`);
});
// END create user END

// BEGIN change user BEGIN
router.put('/:id', function (req, res, next) { 
   const id = getId(req.params); //плюс решить проблему с айди 
   const result = modifyUser(id, req.body); //(ставить самому или требовать от юзера)
   if (result) res.send(`User №${id} was modified successfully`);
   else res.status(400).send(`No user with id = ${id}`);
});
// END change user END

// delete user
router.delete('/:id', function (req, res, next) {
   const id = getId(req.params);
   const toDelete = id - 1;
   // const deletedUser = getName(toDelete);
   const result = deleteData(toDelete);
   if (result) {
      res.send(`User №${toDelete} was removed successfully`);
   } else {
      res.status(400).send(`Some error`);
   }
});
// delete user end



module.exports = router;