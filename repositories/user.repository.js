let fs = require('fs');
let path = require('path');

let filename = path.join(__dirname, '../api-users/userlist.json');

let users = require(filename);

const rwrtFile = () => {
   fs.writeFileSync(filename, JSON.stringify(users, null, 2));
   usersJSON = fs.readFileSync(filename);
   users = JSON.parse(usersJSON);
   console.log(users);
}


const saveData = (data) => {
   if (data) {
      data._id = String(users.length + 1);
      users.push(data);
      rwrtFile();
      return true;
   } else {
      return false;
   }
}


const deleteData = (data) => {
   if (data || data === 0) {
      delete users[data];
      users = users.filter(el => el !== null)
      users.forEach((el, i) => {
         el._id = String(i + 1);
      });
      rwrtFile();
      return true;
   } else {
      return false;
   }
}


const modifyUser = (id, data) => {
   if (users[id - 1]) {
      users[id - 1] = data;
      rwrtFile();
      return true;
   } else {
      return false;
   }
};


module.exports = {
   saveData,
   deleteData,
   modifyUser
};